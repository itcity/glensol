package az.itcity.glensol.web;

import az.itcity.glensol.model.Employee;
import az.itcity.glensol.repository.EmployeeRepository;
import az.itcity.glensol.util.ExcelFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;


@RestController
public class MainController {

    @Value("${upload.dir}")
    private String uploadDir;

    @Autowired
    private EmployeeRepository repository;

    @RequestMapping("/")
    public String index() {
        return "Hello, Glensol";
    }

    @RequestMapping("/employee")
    public ResponseEntity<List<Employee>>  getEmployeeList() {
        return new ResponseEntity(repository.getAllEmployees(), HttpStatus.OK);
    }

    @RequestMapping("/employees")
    public ResponseEntity<PagingData>  getEmployeeListWithPaging() {
        PagingData data = new PagingData();
        data.setDraw(1);
        data.setData(repository.getAllEmployees());
        data.setRecordsTotal(20);
        data.setRecordsFiltered(20);

        return new ResponseEntity(data, HttpStatus.OK);
    }

    @PostMapping("/upload")
    public ResponseEntity<String>  upload(@RequestParam("file") MultipartFile file) {

        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        if(fileName.contains("..")) {
            return new ResponseEntity("File name contains invalid characters!", HttpStatus.BAD_REQUEST);
        }

        if (file.isEmpty()) {
            return new ResponseEntity("Please select a file or file is empty!", HttpStatus.BAD_REQUEST);
        }

        if(fileName.endsWith(".csv") || fileName.endsWith(".xls") || fileName.endsWith(".xlsx")) {
            try {
                byte[] bytes = file.getBytes();
                Path path = Paths.get(uploadDir + file.getOriginalFilename());
                Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
                System.out.println(path);

                List<Employee> employees = ExcelFileReader.readUsers(path.toFile().getAbsolutePath());

                System.out.println(employees);

            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity("Could not upload file.", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity("Only CSV, XLS or XLSX files are supported!", HttpStatus.BAD_REQUEST);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create("/"));
        return new ResponseEntity<>(headers, HttpStatus.MOVED_PERMANENTLY);

//        return new ResponseEntity("File successfully uploaded!", HttpStatus.OK);
    }

    @GetMapping("/download")
    @ResponseBody
    public ResponseEntity<Resource> download() {
        Resource file = null;

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"users.csv\"")
                .body(file);
    }

}
