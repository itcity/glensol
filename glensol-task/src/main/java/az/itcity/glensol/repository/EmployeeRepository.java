package az.itcity.glensol.repository;

import az.itcity.glensol.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class EmployeeRepository {

    private final static Logger LOG = Logger.getLogger(EmployeeRepository.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Transactional(readOnly = true)
    public List<Employee> getAllEmployees() {

        List<Employee> employees = new ArrayList<>();

        jdbcTemplate.query(
                "select id, first_name, last_name, phone_number, salary, salary/(select sum(salary) from employee) as salary_ratio from employee ",
                (rs, rowNum) -> new Employee(
                        rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("phone_number"),
                        rs.getBigDecimal("salary"),
                        rs.getBigDecimal("salary_ratio"))
        ).forEach(employee -> {
            LOG.info(employee.toString());
            employees.add(employee);
        });

        return employees;
    }

    public void addEmployee(Employee employee) {
        String sql = "insert into employee(first_name, last_name, phone_number, salary) VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(
                sql,
                employee.getFirstName(),
                employee.getLastName(),
                employee.getPhoneNumber(),
                employee.getSalary()
        );
    }

    public void updateEmployee(Employee employee) {
        String sql = "update employee set first_name = ?, last_name = ?, phone_number = ?, salary = ? where id = ?";
        jdbcTemplate.update(
                sql,
                employee.getFirstName(),
                employee.getLastName(),
                employee.getPhoneNumber(),
                employee.getSalary(),
                employee.getId()
        );
    }
}
