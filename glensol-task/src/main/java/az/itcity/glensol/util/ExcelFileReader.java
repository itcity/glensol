package az.itcity.glensol.util;

import az.itcity.glensol.model.Employee;
import org.apache.commons.validator.GenericValidator;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class ExcelFileReader {

    public static List<Employee> readUsers(String file) throws Exception {
        List<Employee> employees = new ArrayList<>();

        FileInputStream inputStream = new FileInputStream(new File(file));
        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheetAt(0);
        sheet.forEach(row -> {

            DataFormatter formatter = new DataFormatter();


            System.out.println(formatter.formatCellValue(row.getCell(0)) + " "
                    + formatter.formatCellValue(row.getCell(1)) + " "
                    + formatter.formatCellValue(row.getCell(2)) + " "
                    + formatter.formatCellValue(row.getCell(3))
            );

            Employee employee = new Employee();
            int counter = 0;

            String firstName = formatter.formatCellValue(row.getCell(0));
            if(!GenericValidator.isBlankOrNull(firstName)) {
                employee.setFirstName(firstName);
                counter++;
            }

            String lastName = formatter.formatCellValue(row.getCell(1));
            if(!GenericValidator.isBlankOrNull(lastName)) {
                employee.setLastName(lastName);
                counter++;
            }

            String phoneNumber = formatter.formatCellValue(row.getCell(2));
            if(!GenericValidator.isBlankOrNull(phoneNumber)) {
                employee.setPhoneNumber(phoneNumber);
                counter++;
            }

            String salary = formatter.formatCellValue(row.getCell(3));
            if(!GenericValidator.isBlankOrNull(salary) && GenericValidator.isDouble(salary)) {
                employee.setSalary(BigDecimal.valueOf(Double.parseDouble(salary)));
                counter++;
            }

            if(counter == 4) {
                employees.add(employee);
            } else {
                System.out.println("Skipping row " + row.getRowNum());
            }
        });

        return employees;
    }
}
