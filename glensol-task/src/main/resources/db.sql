create table employee (
    id int not null auto_increment,
    first_name varchar(100),
    last_name varchar(100),
    phone_number varchar(100),
    salary decimal,
    primary key('id')
);